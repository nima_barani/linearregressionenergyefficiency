# Linear Regression Model to Predict Heating Load and Cooling Load Requirements of Buildings

This project aims to predict heating load and cooling load requirements of buildings. I use Linear Regression to train this ML model.

I use 70 percents of Iris dataset for training model and 30 percents for validating.

For evaluating the model, I calculated Mean Squared Error- MSE on both training and validating sets.

![MSE](https://www.datavedas.com/wp-content/uploads/2018/04/image003-1.png)

### Prerequisites

> [TensorFlow](https://www.tensorflow.org/install)
