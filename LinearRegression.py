import tensorflow as tf
import numpy as np
import pandas as pd
from sklearn import preprocessing

rng = np.random
dataset = pd.read_excel('ENB2012_data.xlsx', header=None)
dataset = dataset.drop(dataset.index[0])
dataframe = dataset.reindex(np.random.permutation(dataset.index))
dvalues = dataframe.values.astype(float)
min_max_scaler = preprocessing.MinMaxScaler()
x_scaled = min_max_scaler.fit_transform(dvalues)
df_normalized = pd.DataFrame(x_scaled)
instance_number = df_normalized.shape[0]
train_number = int(instance_number * 0.7)
text_number = instance_number - train_number

features = df_normalized[[0,1,2,3,4,5,6,7]]
labels = df_normalized[[8]]

learning_rate = 0.001
display_step = 50

train_X = features.head(train_number)
train_Y = labels.head(train_number)
n_samples = train_X.shape[0]

X = tf.placeholder(shape=[None, 8], dtype=tf.float32)
Y = tf.placeholder(shape=[None, 1], dtype=tf.float32)

W = tf.Variable(tf.zeros([8,1]), name="W")
b = tf.Variable(tf.zeros([1]), name="b")

Z = tf.add(tf.matmul(X, W), b)

cost = tf.reduce_mean(tf.square(Z-Y))

optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)

init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)
    for step in range(1000):
        sess.run(optimizer, feed_dict={X: train_X, Y: train_Y})

    training_cost = sess.run(cost, feed_dict={X: train_X, Y: train_Y})
    print("Training cost=", training_cost)


    test_X = features.tail(text_number)
    test_Y = labels.tail(text_number)

    testing_cost = sess.run(
        tf.reduce_sum(tf.pow(Z - Y, 2)) / (2 * test_X.shape[0]),
        feed_dict={X: test_X, Y: test_Y})
    print("Testing cost=", testing_cost)
    print("Absolute mean square loss difference:", abs(training_cost - testing_cost))
